function main() {
    var show_info_button = document.getElementsByClassName("show_info")[0];
    var additional_info = document.getElementsByClassName("additional_info")[0];

    function show_additional_info() {
        additional_info.classList.toggle("additional_info_show");
        additional_info.scrollIntoView("smooth");
        show_info_button.classList.toggle("show_info_pressed");
    }

    show_info_button.addEventListener("click", show_additional_info);
}

document.addEventListener("DOMContentLoaded", main);